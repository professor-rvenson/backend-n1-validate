FROM alpine:latest
RUN apk add --no-cache bash curl
WORKDIR /app
COPY validate.sh .
RUN chmod +x validate.sh
RUN ln -s /app/validate.sh /usr/local/bin/validate
ARG SERVER_URL
ENV SERVER_URL $SERVER_URL
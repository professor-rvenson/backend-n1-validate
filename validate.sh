#!/bin/bash

# Check if the required environment variables are set
if [ -z "$STUDENT_NAME" ] || [ -z "$STUDENT_ID" ] || [ -z "$SERVER_URL" ]; then
  echo "Error: STUDENT_NAME, STUDENT_ID, and SERVER_URL environment variables must be set."
  echo "$STUDENT_NAME"
  echo "$STUDENT_ID"
  echo "$SERVER_URL"
  exit 1
fi

# Array to store validation results
VALIDATIONS=()

# Check for folders named exercicio01 to exercicio10
for i in $(seq -w 1 10); do
    FOLDER="exercicio$i"
    if [ -d "$FOLDER" ]; then
        VALIDATIONS+=("\"$FOLDER\"")
    fi
done

# Check for README.md file
if [ -f "README.md" ]; then
    VALIDATIONS+=("\"README.md\"")
fi

# Generate JSON
JSON=$(cat <<EOF
{
  "student_name": "$STUDENT_NAME",
  "student_id": "$STUDENT_ID",
  "validations": [$(IFS=,; echo "${VALIDATIONS[*]}")]
}
EOF
)

# Output JSON to file
echo "$JSON" > validation_results.json

# Send JSON file via POST request using curl
curl -X POST -H "Content-Type: application/json" -d @"validation_results.json" "$SERVER_URL"

echo "Validation completed. JSON saved to validation_results.json and sent to $SERVER_URL."
